<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="imgs/favicon.ico" />
    <title>Trader Moe's - Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	    <style type="text/css">
.bs-example{
	font-family: sans-serif;
	position: relative;
	margin: 50px;
}
.typeahead, .tt-query, .tt-hint {
	border: 2px solid #CCCCCC;
	border-radius: 8px;
	font-size: 24px;
	height: 30px;
	line-height: 30px;
	outline: medium none;
	padding: 8px 12px;
	width: 396px;
}
.typeahead {
	background-color: #FFFFFF;
}
.typeahead:focus {
	border: 2px solid #0097CF;
}
.tt-query {
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
}
.tt-hint {
	color: #999999;
}
.tt-dropdown-menu {
	background-color: #FFFFFF;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 8px;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	margin-top: 12px;
	padding: 8px 0;
	width: 422px;
}
.tt-suggestion {
	font-size: 24px;
	line-height: 24px;
	padding: 3px 20px;
}
.tt-suggestion.tt-is-under-cursor {
	background-color: #0097CF;
	color: #FFFFFF;
}
.tt-suggestion p {
	margin: 0;
}
</style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" style="vertical-align: top"><img style="vertical-align: top; max-width: 250px" src="imgs/traderMoes.png" alt=""></a>
            </div>
            
            
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="login.php">Login</a>						<!-- Change to use PHP to know and display user information || Default value to Login -->
                    </li>
                    <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products</a>
                
                <ul class="dropdown-menu">

            <?php
				$servername = "localhost";		//Even though it is on Cobweb this is way it works
	            $username = "tjmagnan";
	            $password = "AiER6ucT";
	            $dbname = "tjmagnan";

	            // Create connection
	            $conn = new mysqli($servername, $username, $password, $dbname);
	            // Check connection
	            if ($conn->connect_error) {
	              die("Connection failed: " . $conn->connect_error);
	            } 

	            $sql = "SELECT DISTINCT Category FROM products WHERE quantity > 0";

	            $result = $conn->query($sql);

			    if ($result->num_rows > 0)
			    {
			   
			        while ($row = $result->fetch_assoc()) {

				        $cat = $row["Category"];

				        echo '<li>';
		                echo '<a href="#">'.$cat.'</a>'; 
		                echo '</li>';
	                }

			    }
			    else {
			      	echo '<li>';
		            echo '<a href="#">No Products Available</a>'; 
		            echo '</li>';
			    }

			    $conn->close();
			?>
                </ul>
                    <li>
                        <a href="cart.php">Cart</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

	<div class="container">
  		<h2>Your Cart</h2>
<!-- 
  		<p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>   
 -->
 
 		  		         
  		<table class="table">
    		<thead>
     			<tr>
        		<th>Product</th>
        		<th>Price</th>
        		<th>Quantity</th>
      			</tr>
    		</thead>
  		
  		<?php
			$servername = "localhost";
            $username = "tjmagnan";
            $password = "AiER6ucT";
            $dbname = "tjmagnan";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            } 


			//CHANGE to query of only items for certain username
				//If not logged in have show empty cart or login screen not error
            $sql = "SELECT * FROM cart WHERE quantity > 0";

            $result = $conn->query($sql);

		    if ($result->num_rows > 0)
		    {
		   
		        while ($row = $result->fetch_assoc()) {

		        	echo '
		        		
    				<tbody>
     					<tr>	
		        	';

	                  $name = $row["pName"];
	                  $nameLen = strlen($row["pName"]);
			          $price = $row["price"];
			          $quantity = $row["quantity"];

	                  echo "<td>".$name."</td>";	
	                  echo "<td>".$price."</td>";
	                  echo "<td>".$quantity."</td>";
	                  echo '
      						</tr>
   						</tbody>
	                  ';
                }


		    }
		    else {
		      echo "You have no items in your cart";
		    }

		    $conn->close();
	?>

<!-- 
		Before we close the table have a total of all items in cart :: HOW?
 -->

 		</table>
	</div>
	
<!-- 
		Submit to store button	
			- Email to themselves
			- Email to store
 -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align: center">&copy; Copyright 2016 | Davis, Magnan & Acosta</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- Live Search -->
	<script src="js/typeahead.min.js"></script>
    <script>
    $(document).ready(function(){
    $('input.typeahead').typeahead({
        name: 'typeahead',
        remote:'search.php?key=%QUERY',
        limit : 10
    });
});
    </script>

</body>

</html>
