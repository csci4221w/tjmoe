<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trader Joes - Make your reservation</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" ><img style="vertical-align: center; max-width: 200px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Trader_Joes_Logo.svg/2000px-Trader_Joes_Logo.svg.png" alt=""></a>
            </div>
            
            
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">How to Order</a>
                    </li>
                    <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products</a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#">Grains</a>
                    </li>
                    <li>
                        <a href="#">Fruits & Vegtables</a>
                    </li>
                    <li>
                        <a href="#">Snacks</a>
                    </li>
                    <li>
                        <a href="#">Dairy</a>
                    </li>
                    <li>
                        <a href="#">Protien</a>
                    </li>
                </ul>
                    <li>
                        <a href="#">Cart</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
            <h1 style="text-align: center">Welcome to Trader Joes!</h1>
            <p>Take a moment to look at our inventory and place or order so it will be ready for pick up when you arrive!</p>
            <p>No need to wonder if your product won't be there, check stock online.</p>
            <p><a class="btn btn-primary btn-large">Search</a>
            </p>
        </header>

        <hr>

        <!-- Title -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Current Inventory</h3>
            </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        <div class="row text-center">
            
            
            <!-- Replace static table with PHP loop of each item in the table -->

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                        <h3>Item Name</h3>
                        <p>Category:</p>
                        <p>Description:</p>
                        <p>Price:</p>
                        <p>Quatity Remaining:</p>
                        <p>
                            <a href="#" class="btn btn-primary">Add to Cart</a>
                        </p>
                    </div>
                </div>
            </div>
            
<!--
            <?php
                $servername = "localhost";      //cobweb.seas.gwu.edu
                $username = "tjmagnan";         //tjmagnan    
                $password = "AiER6ucT";         //SEAS password
                $database = "tjmagnan";         //tjmagnan for Cobweb

                //Create connection
                $conn = mysqli_connect($servername, $username, $password, $database);

                if (!$conn) {
                    die("Connection failed: " . mysqli_connect_error());
                }

                $sql = "SELECT * FROM products";
                $result = mysqli_query($conn, $sql);


                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while($row = mysqli_fetch_assoc($result)) {
                        $pName = $row["pName"];
                        $Category = $row["Category"]
                        $price = $row["price"]
                        $quantity = $row["qauantity"]
                        $pictureAddress = $row["pictureAddress"]

                        echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";

                        <div class="col-md-3 col-sm-6 hero-feature">
                            <div class="thumbnail">
                                <img src="$pictureAddress" alt="">
                                <div class="caption">
                                    <h3>$pName</h3>
                                    <p>Category: $Category</p>
                                    <p>Price: $$price</p>
                                    <p>Quatity Remaining: $quantity</p>
                                    <p>
                                        <a href="#" class="btn btn-primary">Add to Cart</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    }
                } else {
                    echo "0 results";
                }

                mysqli_close($conn);
            ?>
-->
            

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Magnan, Acosta & Davis 2016</p>
                    <p>Note: There is no affiliation with Trader Joe's.  Purely done for academic purposes.</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
